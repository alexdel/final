import array

import math

import soundfile

import config


def _samples(duration):

    # Total number of samples (samples per second by number of seconds)
    return int(config.samples_second * duration)

def sin(duration: float, freq: float):

    nsamples= _samples(duration)

    # Create an array of integers (signed shorts: 16 bits signed integers)
    sound = array.array("h",[0] * nsamples)

    for nsample in range(nsamples):
        t= nsample / config.samples_second
        sound[nsample] = int(config.max_amp * math.sin(2 * config.pi * freq * t))

    return sound

def constant(duration: float, positive: bool):

    nsamples = _samples(duration)

    # create an array of integers with either max or sin amplitude

    if positive:

        sample = config.max_amp

    else:

        sample = - config.max_amp

    sound = array.array("h", [sample] * nsamples)

    return sound


def square(duration: float, freq: float):

    nsamples = _samples(duration)
    sound = array.array("h",[0]* nsamples)

    x1 = 1

    for nsample in range(nsamples):

        if nsample % freq == 0:

            x1 *= -1

        sound[nsample] = (config.max_amp * x1)

    return sound

def readfile(duration: float, path: str):

    nsamples= int(config.samples_second * duration)
    data, config.samples_seconds = soundfile.read(path, dtype="int16")

    if len(data) >= nsamples:

        sound = array.array("h", [0] * nsamples)

        try:

            for nsample in range(len(sound)):

                sound[nsample] = data[nsample, 0]

            return sound

        except:

            for nsample in range(len(sound)):

                sound[nsample] = data[nsample]

            return sound
    else:

        sound = array.array("h", [0] * len(data))

        try:

            for nsample in range(len(sound)):

                sound[nsample] = data[nsample, 0]

            return sound

        except:

            for nsample in range(len(sound)):

                sound[nsample] = data[nsample]

            return sound



