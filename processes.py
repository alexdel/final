import sys

import config


def ampli(sound, factor: float):

    nsamples = len(sound)

    if factor >0:

        for i in range(nsamples):

            n1 = int(sound[i] * factor)

            if n1 > config.max_amp:
                sound[i] = config.max_amp

            elif n1 < config.min_amp:
                sound[i] = config.min_amp

            else:
                sound[i] = n1

        return sound

    else:

        sys.exit("ERROR.")

def reduce(sound, factor: int):

    if factor > 0:

        del sound[factor-1::factor]

        return sound

    else:
        print("Error.")


def extend(sound, factor: int):


    nsamples = len(sound)

    n=0
    if factor > 0:

        for i in range(factor, nsamples, factor):

            if i > factor:

                n += 1
                i = i+n

                sound.insert(i, (sound[i - 1] + sound[i]) // 2)

            else:
                sound.insert(i, (sound[i - 1] + sound[i]) // 2)

        return sound

    else:

        print("Error extend.")


def round(sound, samples: int):

    nsamples = len(sound)

    if samples > 0:

        for x in range(0, nsamples):
            if x < samples:

                a = 0
                b = samples + x
                c = 0

                while b >= 0:

                    a += sound[b]
                    b -= 1
                    c += 1

                a = a // c
                sound[x] = a

            elif x >= samples:

                if x >= nsamples - samples:

                    a = 0
                    b = x - samples
                    c = 0

                    while b < nsamples:
                        a += sound[b]
                        b += 1
                        c += 1

                    a = a // (c)
                    sound[x] = a

                else:

                    a = 0

                    for l in range(x - samples, x+samples+1):

                        a += sound[l]

                    a = a // ((samples * 2) + 1)
                    sound[x] = a

    else:

        sys.exit("Error")
